[GtkTemplate(ui = "/top/yesion/mywindow/env-list-row.ui")]
class EnvListRow : Gtk.ListBoxRow {
    [GtkChild]
    private unowned Gtk.Label lab_env_name;
    [GtkChild]
    private unowned Gtk.Label lab_env_value;

    public unowned string env_name {
        get {
            return lab_env_name.get_text ();
        }
    }

    public unowned string env_value {
        get {
            return lab_env_value.get_text ();
        }
    }

    public EnvListRow(string name, string value) {
        this.lab_env_name.set_text(name);
        this.lab_env_value.set_text(value);
    }

}