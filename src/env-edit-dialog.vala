[GtkTemplate(ui = "/top/yesion/mywindow/env-edit-dialog.ui")]
class EnvEditDialog : Gtk.Dialog {
    [GtkChild]
    private unowned Gtk.Entry entry_env_name;
    [GtkChild]
    private unowned Gtk.Entry entry_env_value;

    public signal void sig_save(string env_name, string env_value);

    public EnvEditDialog.with_update(string name, string value) {
        this.entry_env_name.set_text (name);
        this.entry_env_value.set_text (value);
    }

    [GtkCallback]
    private void btn_cancel_clicked_cb() {
        this.close();
    }

    [GtkCallback]
    private void btn_ok_clicked_cb() {
        if (entry_env_name.text.strip() == "" || entry_env_value.text.strip() == "") {
            var dig = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.CLOSE, "变量名称或者值不能为空");
            dig.run();
            dig.destroy();
            return;
        }
        this.sig_save(entry_env_name.get_text().strip(), entry_env_value.get_text().strip());
        this.close();
    }

    [GtkCallback]
    private void btn_choose_file_clicked_cb() {
        var dig = new Gtk.FileChooserDialog("选择文件", this, Gtk.FileChooserAction.OPEN, "取消", Gtk.ResponseType.CANCEL, "打开", Gtk.ResponseType.OK);
        if (dig.run() == Gtk.ResponseType.OK) {
            try {
                string path_uri = dig.get_uri();
                var turi = Uri.parse(path_uri, GLib.UriFlags.ENCODED_PATH);
                string s = Uri.unescape_string(turi.get_path(), null);
                message(@"$(s)");
                this.entry_env_value.set_text(s);
            } catch (Error e) {
                message(e.message);
            }
        }
        dig.destroy();
        
    }

    [GtkCallback]
    private void btn_choose_folder_clicked_cb() {
        var dig = new Gtk.FileChooserDialog("选择目录", this, Gtk.FileChooserAction.SELECT_FOLDER, "取消", Gtk.ResponseType.CANCEL, "打开", Gtk.ResponseType.OK);
        if (dig.run() == Gtk.ResponseType.OK) {
            try {
                string path_uri = dig.get_uri();
                var turi = Uri.parse(path_uri, GLib.UriFlags.ENCODED_PATH);
                string s = Uri.unescape_string(turi.get_path(), null);
                message(@"$(s)");
                this.entry_env_value.set_text(s);
            } catch (Error e) {
                message(e.message);
            }
        }
        dig.destroy();
    }
}