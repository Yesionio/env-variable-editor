class NameValue : Object {
    public string name { get; set; }
    public string value { get; set; }

    public NameValue(string n, string v) {
        Object(name: n, value: v);
    }
}

[GtkTemplate(ui = "/top/yesion/mywindow/main-window.ui")]
class MainWindow : Gtk.Window {
    [GtkChild]
    private unowned Gtk.ListBox env_list;

    private List<NameValue?> var_list = new List<NameValue?>();
    private string file_path = Environment.get_user_config_dir() + "/" + "env_variable_config.ini";
    private bool published;
    private ListStore listStore = new ListStore(typeof(NameValue));

    // 构造函数
    public MainWindow() {
        read_config_file();
        refresh_list();
        this.destroy.connect(this.write_config_file);
        env_list.bind_model(listStore, (item) => {
            var nv = item as NameValue;
            return new EnvListRow(nv.name, nv.value);
        });
    }

    // 点击添加按钮时的回调
    [GtkCallback]
    private void btn_add_clicked_cb() {
        var dig = new EnvEditDialog();
        dig.set_transient_for(this);
        dig.sig_save.connect((env_name, env_value) => {
            message("添加成功");
            var nv = new NameValue(env_name, env_value);
            var_list.append(nv);
            refresh_list();
        });
        dig.show();        
    }

    // 点击编辑按钮时的回调
    [GtkCallback]
    private void btn_edit_clicked_cb() {
        unowned var row = this.env_list.get_selected_row();
        if (row is EnvListRow) {
            unowned var env_row = row as EnvListRow;
            var dig = new EnvEditDialog.with_update(env_row.env_name, env_row.env_value);
            dig.set_transient_for(this);
            dig.sig_save.connect((env_name, env_value) => {
                message("编辑成功, %d", row.get_index());
                int idx = row.get_index();
                unowned var nv = var_list.nth_data(idx);
                nv.name = env_name;
                nv.value = env_value;
                refresh_list();
            });
            dig.show();
        }
    }

    // 点击删除按钮时的回调
    [GtkCallback]
    private void btn_remove_clicked_cb() {
        unowned var row = this.env_list.get_selected_row();
        if (row is EnvListRow) {
            int idx = row.get_index();
            var dig = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, "是否要删除这个变量");
            int rls = dig.run();
            if (rls == Gtk.ResponseType.YES) {
                var_list.remove(var_list.nth_data(idx));
                refresh_list();
            } else {
                message("取消操作");
            }
            dig.destroy();
        }
    }

    // 点击发布&预览按钮时的回调
    [GtkCallback]
    public void btn_pub_clicked_cb() {
        var dig = new PreviewDialog(this, var_list);
        var resp = dig.run();
        if (resp == Gtk.ResponseType.CANCEL) {
            message("取消操作");
        } else {
            message("操作成功");
        }

        dig.destroy();
    }

    [GtkCallback]
    private void env_list_row_activated_cb(Gtk.ListBoxRow row) {
        btn_edit_clicked_cb();
    }

    // 从文件中读取配置
    private void read_config_file() {
        KeyFile keyFile = new KeyFile();
        string group_name = "variable_list";
        try {
            keyFile.load_from_file(file_path, GLib.KeyFileFlags.KEEP_COMMENTS);
            published = keyFile.get_boolean("state", "published");
            foreach (var key in keyFile.get_keys(group_name)) {
                string value = keyFile.get_string(group_name, key);
                var obj = new NameValue(key, value);
                var_list.append(obj);
            }
        } catch (Error e) {
            message(e.message);
        }
    }

    // 写入配置到文件
    private void write_config_file() {
        KeyFile keyFile = new KeyFile();
        string group_name = "variable_list";
        keyFile.set_boolean("state", "published", published);
        foreach (var item in var_list) {
            keyFile.set_string(group_name, item.name, item.value);
        }
        try {
            keyFile.save_to_file(file_path);
        } catch(Error e) {
            message("保存失败: %s", e.message);
        }
    }

    // 刷新列表
    private void refresh_list() {
        listStore.remove_all();
        foreach (var item in var_list) {
            listStore.append(item);
        }
    }
}