[GtkTemplate(ui = "/top/yesion/mywindow/preview-dialog.ui")]
class PreviewDialog : Gtk.Dialog {
    [GtkChild]
    private unowned Gtk.TextBuffer txbuf_preview;

    public PreviewDialog(Gtk.Window? parent, List<NameValue> nameValueList) {
        this.set_transient_for(parent);
        StringBuilder sb = new StringBuilder ("");
        foreach (var item in nameValueList) {
            sb.append_printf ("export %s=\"%s\"\n", item.name, item.value);
        }
        txbuf_preview.set_text (sb.str);
    }

    [GtkCallback]
    private void btn_cancel_clicked_cb() {
        this.response(Gtk.ResponseType.CANCEL);
    }

    [GtkCallback]
    private void btn_pub_clicked_cb() {
        string dir = Environment.get_home_dir();
        File f = File.new_for_path(dir + "/.bash_env");
        try {
            var fos = f.replace(null, false, GLib.FileCreateFlags.NONE, null);
            fos.write(txbuf_preview.text.data, null);
            fos.close(null);
        } catch (Error e) {
            var dig = new Gtk.MessageDialog(this, Gtk.DialogFlags.DESTROY_WITH_PARENT, Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE, "发生错误: %s", e.message);
            dig.run();
            dig.destroy();
            return;
        }
        this.response(Gtk.ResponseType.APPLY);
    }
}